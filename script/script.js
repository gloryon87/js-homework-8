'use strict'

// 1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.getElementsByTagName('p')
for (let paragraph of paragraphs) {
    paragraph.style.background = '#ff0000'
}

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const optionsList = document.getElementById('optionsList')
console.log(optionsList);
console.log('parent', optionsList.parentElement);
console.log('optionsList childNodes');
for (const node of optionsList.childNodes) {
    console.log(node.nodeName, node.nodeType);
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph

document.getElementById('testParagraph').textContent = 'This is a paragraph'

// 4. Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const li = document.getElementsByClassName('main-header')[0].querySelectorAll('li')

console.log('main-header li');
for (const i of li) {
    console.log(i);
    i.className += " nav-item"
}

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitle = document.querySelectorAll('.section-title')
for (const i of sectionTitle) {
    i.classList.remove("section-title")
}